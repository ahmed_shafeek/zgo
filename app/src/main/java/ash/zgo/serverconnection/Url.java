package ash.zgo.serverconnection;

public class Url {

	private static Url url = new Url();

	public static Url getInstance( ) {
		return url;
	}

	final public String clientRegistrationURL="http://www.maneedz.com/api/ClientUser/Register";
	final public String serviceProviderRegistrationURL="http://www.maneedz.com/api/SPUser/Register";
	final public String loginURL="http://www.maneedz.com/api/SPUser/Login";
	final public String activationURL="http://www.maneedz.com/api/SPUser/Activation";
	final public String categoriesURL="http://maneedz.com/api/ClientUser/GetCategories";
	final public String addCategoriesURL="http://maneedz.com/api/SPUser/AddCategories";
	final public String removeCategoriesURL="http://maneedz.com/api/SPUser/RemoveCategories";
	final public String getSelectedcategoriesURL="http://maneedz.com/api/SPUser/GetCategories";
	final public String categoriesImageURL="http://www.maneedz.com/Uploads/Categories";
	final public String socialMediasImageURL="http://www.maneedz.com/Uploads/SocialMedias";
	final public String profilePicturesURL="http://www.maneedz.com/Uploads/ProfilePictures/";
	final public String newsPicturesURL="http://www.maneedz.com/Uploads/News/";
	final public String serviceListURL="http://www.maneedz.com/api/User/GetServiceListSummary";
	final public String searchServiceListSummaryURL="http://www.maneedz.com/api/User/SearchServiceListSummary";
	final public String callMeURL="http://www.maneedz.com/api/User/CallMe";
	final public String sendRequestURL="http://www.maneedz.com/api/User/SendLocation";
	final public String sendComplainsURL="http://www.maneedz.com/api/User/SendComplains";
	final public String addReviewURL="http://www.maneedz.com/api/User/AddReview";
	final public String getServiceProviderReviewsURL="http://www.maneedz.com/api/ClientUser/GetServiceProviderReviews";
	final public String updateUserProfileURL="http://www.maneedz.com/api/User/updateUserProfile";
	final public String updateUserProfilePicURL="http://www.maneedz.com/api/User/UpdateUserProfilePic";
	final public String updateUserProfilePasswordURL="http://www.maneedz.com/api/User/updateUserProfilePassword";
	final public String workHoursURL="http://www.maneedz.com/api/ClientUser/GetServiceProviderHourWorks";
	final public String showLocationRequestsURL="http://www.maneedz.com/api/SPUser/ShowLocationRequests";
	final public String ShowCallRequeststsURL="http://www.maneedz.com/api/SPUser/ShowCallRequests";
	final public String showClientReviewsURL="http://www.maneedz.com/api/SPUser/ShowClientReview";
	final public String getWorkHoursURL="http://www.maneedz.com/api/SPUser/GetWorkHours";
	final public String updateWorkHoursURL="http://www.maneedz.com/api/SPUser/UpdateWorkHours";
	final public String getPriceListURL="http://www.maneedz.com/api/SPUser/ShowPriceList";
	final public String getNewsURL="http://www.maneedz.com/api/SPUser/ShowNews";
	final public String searchGetCategoriesURL="http://www.maneedz.com/api/ClientUser/SearchGetCategories";
	final public String getServiceProviderByidURL="http://www.maneedz.com/api/User/GetServiceProviderByid";
	final public String socialMediaURL="http://www.maneedz.com/api/User/GetSpUserSocialMedias";
	final public String getsocialMediaURL="http://www.maneedz.com/api/SPUser/GetSpUserSocialMedias";
	final public String getAllSocialMediaURL="http://www.maneedz.com/api/SPUser/ShowSocialMedia";
	final public String addSocialMediaURL="http://www.maneedz.com/api/SPUser/AddSpUserSocialMedia";
	final public String deleteSocialMediaURL="http://www.maneedz.com/api/SPUser/DeleteSpUserSocialMedia";
	final public String updateSocialMediaURL="http://www.maneedz.com/api/SPUser/UpdateSpUserSocialMedia";
}