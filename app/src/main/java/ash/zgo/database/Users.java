package ash.zgo.database;

import com.orm.SugarRecord;
import com.orm.dsl.Table;
import com.orm.dsl.Unique;

/**
 * Created by ahmed on 3/7/17.
 */

public class Users extends SugarRecord {
    @Unique
    public String username;
    public String password;
    public String email;
    public String photo;

    public Users() {
    }

    public Users(String username, String password, String email, String photo) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.photo = photo;
    }
}
