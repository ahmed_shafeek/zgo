package ash.zgo.database;

import com.orm.SugarRecord;

import org.w3c.dom.Document;

/**
 * Created by ahmed on 3/7/17.
 */

public class Locations extends SugarRecord {

    public String title;
    public String latSrc;
    public String lngSrc;
    public String latDes;
    public String lngDes;
    public String distance;
    public String time;
    public String document;

    public Locations() {
    }

    public Locations(String title, String latSrc, String lngSrc, String latDes, String lngDes, String distance, String time, String document) {
        this.title = title;
        this.latSrc = latSrc;
        this.lngSrc = lngSrc;
        this.latDes = latDes;
        this.lngDes = lngDes;
        this.distance = distance;
        this.time = time;
        this.document = document;
    }
}
