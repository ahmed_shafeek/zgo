package ash.zgo.fragments;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.w3c.dom.Document;

import java.io.StringWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import ash.zgo.R;
import ash.zgo.database.Locations;
import ash.zgo.utils.CasheMap;
import ash.zgo.utils.GMapV2Direction;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by ahmed on 3/6/17.
 */

public class HomeFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMapClickListener {
    @BindView(R.id.sourceTextView) TextView sourceTextView;
    @BindView(R.id.destinationTextView) TextView destinationTextView;
    @BindView(R.id.distanceTextView) TextView distanceTextView;
    @BindView(R.id.timeTextView) TextView timeTextView;
    View view;
    int REQUEST_CODE_AUTOCOMPLETE = 1;
    //double latSrc, lngSrc, latDes, lngDes;
    CasheMap casheMap;
    int type;
    SupportMapFragment mMapFragment;
    FragmentTransaction fragmentTransaction;
    GoogleMap googleMap;
    Dialog dialog;
    PolylineOptions polylineOptions;
    GMapV2Direction gMapV2Direction;
    Document document;
    LatLng src, des;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.home_fragment, container, false);
        ButterKnife.bind(this, view);
        initialization();
        return view;
    }

    private void initialization(){
        casheMap = new CasheMap(getActivity());
        mMapFragment = SupportMapFragment.newInstance();
        fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.mapFragment, mMapFragment);
        fragmentTransaction.commit();
        mMapFragment.getMapAsync(this);
    }

    private void openAutocompleteActivity() {
        try {
            Intent intent = new PlaceAutocomplete.IntentBuilder(
                    PlaceAutocomplete.MODE_FULLSCREEN).build(getActivity());
            startActivityForResult(intent, REQUEST_CODE_AUTOCOMPLETE);
        } catch (GooglePlayServicesRepairableException e) {
            GoogleApiAvailability
                    .getInstance()
                    .getErrorDialog(getActivity(), e.getConnectionStatusCode(), 0).show();
        } catch (GooglePlayServicesNotAvailableException e) {
            String message = "Google Play Services is not available: "
                    + GoogleApiAvailability.getInstance().getErrorString(
                    e.errorCode);
            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_AUTOCOMPLETE) {
            if (resultCode == getActivity().RESULT_OK) {
                Place place = PlaceAutocomplete.getPlace(getActivity(), data);
                if (type == 0){
                    sourceTextView.setText(place.getName());
                    casheMap.setLatSrc(place.getLatLng().latitude);
                    casheMap.setLngSrc(place.getLatLng().longitude);
                    mMapFragment.getMapAsync(this);
                } else {
                    destinationTextView.setText(place.getName());
                    casheMap.setLatDes(place.getLatLng().latitude);
                    casheMap.setLngDes(place.getLatLng().longitude);
                    mMapFragment.getMapAsync(this);
                }
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
//                Status status = PlaceAutocomplete.getStatus(this, data);
            } else if (resultCode == getActivity().RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.clear();
        this.googleMap = googleMap;
        try {
            LatLng latLngSrc = new LatLng(casheMap.getLatSrc(getActivity()), casheMap.getLngSrc(getActivity()));
            googleMap.addMarker(new MarkerOptions()
                    //.icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_launcher))
                    .position(latLngSrc));

            LatLng latLngDes = new LatLng(casheMap.getLatDes(getActivity()), casheMap.getLngDes(getActivity()));
            googleMap.addMarker(new MarkerOptions()
                    //.icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_launcher))
                    .position(latLngDes));

            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLngSrc, 15.0f));
            googleMap.setOnMapClickListener(this);
            src = latLngSrc;
            des = latLngDes;
            new GetDirection().execute();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onMapClick(final LatLng latLng) {
        dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.src_or_des_dialog);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Button srcButton = (Button) dialog.findViewById(R.id.srcButton);
        Button desButton = (Button) dialog.findViewById(R.id.desButton);
        srcButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                casheMap.setLatSrc(latLng.latitude);
                casheMap.setLngSrc(latLng.longitude);
                mMapFragment.getMapAsync(HomeFragment.this);
            }
        });
        desButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                casheMap.setLatDes(latLng.latitude);
                casheMap.setLngDes(latLng.longitude);
                mMapFragment.getMapAsync(HomeFragment.this);
            }
        });
        dialog.show();
    }

    @OnClick(R.id.sourceTextView) void sourceAction() {
        type = 0;
        openAutocompleteActivity();
    }

    @OnClick(R.id.destinationTextView) void destinationAction() {
        type = 1;
        openAutocompleteActivity();
    }

    @OnClick(R.id.navigationButton) void navigationAction() {
        final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?" + "saddr="+ casheMap.getLatSrc(getActivity()) + "," + casheMap.getLngSrc(getActivity()) + "&daddr=" + casheMap.getLatDes(getActivity()) + "," + casheMap.getLngDes(getActivity())));
        intent.setClassName("com.google.android.apps.maps","com.google.android.maps.MapsActivity");
        startActivity(intent);
    }

    private class GetDirection extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            gMapV2Direction = new GMapV2Direction();
            document = gMapV2Direction.getDocument(src, des,
                    GMapV2Direction.MODE_DRIVING);
            return "Executed";
        }

        @Override
        protected void onPostExecute(String result) {
            dialog.dismiss();
            try {
                calculateTime(gMapV2Direction.getTimeValue(document));
                calculateDistance(gMapV2Direction.getDistanceValue(document));
                ArrayList<LatLng> directionPoint = gMapV2Direction.getDirection(document);
                polylineOptions = new PolylineOptions().width(5).color(Color.RED);
                for (int i = 0; i < directionPoint.size(); i++)
                    polylineOptions.add(directionPoint.get(i));
                googleMap.addPolyline(polylineOptions);

                DOMSource domSource = new DOMSource(document);
                StringWriter writer = new StringWriter();
                StreamResult resultt = new StreamResult(writer);
                TransformerFactory tf = TransformerFactory.newInstance();
                Transformer transformer = tf.newTransformer();
                transformer.transform(domSource, resultt);

                Locations locations = new Locations("History", casheMap.getLatSrc(getActivity())+"", casheMap.getLngSrc(getActivity())+"",
                        casheMap.getLatDes(getActivity())+"", casheMap.getLngDes(getActivity())+"", calculateDistance(gMapV2Direction.getDistanceValue(document)),
                        calculateTime(gMapV2Direction.getTimeValue(document)), writer.toString());
                locations.save();
            } catch (Exception e){
                e.printStackTrace();
            }
        }

        @Override
        protected void onPreExecute() {
            dialog = new Dialog(getActivity());
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.progress_dialog);
            dialog.setCancelable(false);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();
        }
    }

    private String calculateTime(int time){
        if (time < 60){
            timeTextView.setText("1 " + getString(R.string.min));
        } else if(time > 60 && time < 3600){
            timeTextView.setText(time / 60 + " " + getString(R.string.min));
        } else {
            timeTextView.setText(time / 60 / 60 + " " + getString(R.string.hours));
        }
        return timeTextView.getText().toString();
    }

    private String calculateDistance(int distance){
        if (distance < 1000){
            distanceTextView.setText(distance + " " + getString(R.string.m));
        } else {
            distanceTextView.setText(distance / 1000 + " " + getString(R.string.km));
        }
        return distanceTextView.getText().toString();
    }
}
