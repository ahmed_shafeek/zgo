package ash.zgo.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.MapView;

import ash.zgo.R;
import ash.zgo.adapters.MapLocationAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by ahmed.shafeek on 3/6/2017.
 */

public class HistoryFragment extends Fragment {
    @BindView(R.id.mapList) RecyclerView mapList;
    View view;
    MapLocationAdapter mListAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.history_fragment, container, false);
        ButterKnife.bind(this, view);
        initialization();
        set();
        return view;
    }

    private void initialization(){
        mListAdapter = new MapLocationAdapter(mapList, getActivity());
    }

    private void set(){
        mapList.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        mapList.setAdapter(mListAdapter);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();

        if (mListAdapter != null) {
            for (MapView m : mListAdapter.getMapViews()) {
                m.onLowMemory();
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        if (mListAdapter != null) {
            for (MapView m : mListAdapter.getMapViews()) {
                m.onPause();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());

        if (resultCode == ConnectionResult.SUCCESS) {
            mapList.setAdapter(mListAdapter);
        } else {
            GooglePlayServicesUtil.getErrorDialog(resultCode, getActivity(), 1).show();
        }

        if (mListAdapter != null) {
            for (MapView m : mListAdapter.getMapViews()) {
                m.onResume();
            }
        }
    }

    @Override
    public void onDestroy() {
        if (mListAdapter != null) {
            for (MapView m : mListAdapter.getMapViews()) {
                m.onDestroy();
            }
        }

        super.onDestroy();
    }
}
