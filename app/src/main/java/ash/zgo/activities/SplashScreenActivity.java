package ash.zgo.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;

import org.json.JSONObject;

import ash.zgo.R;
import ash.zgo.utils.LoginSharedPreferences;

public class SplashScreenActivity extends Activity {

    Handler handler;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen_activity);
        waitting();
    }

    private void waitting(){
        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                checkLogin();
            }
        }, 3000);
    }

    private void toRegistration(){
        finish();
        Intent intent = new Intent(this, RegistrationActivity.class);
        startActivity(intent);
    }

    private void toMain(){
        finish();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    private void checkLogin() {
        LoginSharedPreferences loginSharedPreferences = new LoginSharedPreferences();
        if (loginSharedPreferences.getRemember(this).equals("0")) {
            toRegistration();
        } else {
            toMain();
        }
    }
}
