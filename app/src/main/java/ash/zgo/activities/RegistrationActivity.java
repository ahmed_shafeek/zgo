package ash.zgo.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import ash.zgo.R;
import ash.zgo.database.Users;
import ash.zgo.utils.Permissions;
import ash.zgo.utils.Validation.ValidationObject;
import ash.zgo.utils.Validation.ViewsValidation;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegistrationActivity extends Activity {
    @BindView(R.id.emilEditText) EditText emilEditText;
    @BindView(R.id.usernameEditText) EditText usernameEditText;
    @BindView(R.id.passwordEditText) EditText passwordEditText;
    @BindView(R.id.userImageView) ImageView userImageView;
    Permissions permissions;
    int IMAGE_CAPTURE = 1888;
    final static int IMAGE = 1111;
    String imageData = "";
    Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration_activity);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.signInButton) void signInAction() {
        toLogin();
    }

    @OnClick(R.id.registerButton) void registerAction() {
        if (checkValidation()){
            if (imageData != "")
                checkUser();
            else
                Toast.makeText(this, getString(R.string.pleaseUploadImage), Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.uploadTextView) void uploadTextView() {
        if (permissions.hasPermission(this, Manifest.permission.CAMERA)) {
            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(cameraIntent, IMAGE_CAPTURE);
        } else {
            this.requestPermissions(new String[]{Manifest.permission.CAMERA}, IMAGE);
        }
    }

    @OnClick(R.id.userImageView) void userImageViewAction() {
        if (permissions.hasPermission(this, Manifest.permission.CAMERA)) {
            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(cameraIntent, IMAGE_CAPTURE);
        } else {
            this.requestPermissions(new String[]{Manifest.permission.CAMERA}, IMAGE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults){
        switch (requestCode) {
            case IMAGE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(cameraIntent, IMAGE_CAPTURE);
                } else {
//                    permission is denied
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == IMAGE_CAPTURE) {
            if (resultCode == RESULT_OK) {
                handleSmallCameraPhoto(data);
            }
        }
    }

    private void handleSmallCameraPhoto(Intent intent) {
        Bundle extras = intent.getExtras();
        bitmap = (Bitmap) extras.get("data");
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
        byte[] b = baos.toByteArray();
        String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);
        imageData  = encodedImage;
        userImageView.setImageBitmap(bitmap);
        Log.e("Image", imageData);
    }

    private void checkUser(){
        try {
            List<Users> emailsResult = Users.findWithQuery(Users.class, "Select * from Users where email = ?", emilEditText.getText().toString());
            List<Users> usernameResult = Users.findWithQuery(Users.class, "Select * from Users where username = ?", usernameEditText.getText().toString());
            if (emailsResult.size() == 0 && usernameResult.size() == 0){
                Users users = new Users(usernameEditText.getText().toString(), passwordEditText.getText().toString(), emilEditText.getText().toString(), imageData);
                users.save();
                toLogin();
            } else {
                Toast.makeText(this, getString(R.string.userAlreadyExist), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e){
            Users users = new Users(usernameEditText.getText().toString(), passwordEditText.getText().toString(), emilEditText.getText().toString(), imageData);
            users.save();
            toLogin();
            e.printStackTrace();
        } finally {

        }
    }

    public boolean checkValidation() {
        ArrayList<ValidationObject> list=new ArrayList<>();
        list.add(new ValidationObject(usernameEditText , 6, true, R.string.usernameErrorMsg));
        list.add(new ValidationObject(passwordEditText , 6, true, R.string.passwordErrorMsg));
        list.add(new ValidationObject(emilEditText , true, R.string.emailErrorMsg));
        return ViewsValidation.getInstance().validate(this,list);
    }

    private void toMainActivity(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    private void toLogin(){
        finish();
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }
}
