package ash.zgo.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import ash.zgo.R;
import ash.zgo.adapters.MainMenuAdapter;
import ash.zgo.fragments.HistoryFragment;
import ash.zgo.fragments.HomeFragment;
import ash.zgo.models.MainMenuModel;
import ash.zgo.utils.LoginSharedPreferences;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends FragmentActivity implements AdapterView.OnItemClickListener {
    @BindView(R.id.menuImageView) ImageView menuImageView;
    @BindView(R.id.drawerLayout) DrawerLayout drawerLayout;
    @BindView(R.id.menuListView) ListView menuListView;
    ArrayList<MainMenuModel> mainMenuModels;
    View mainMenuHeader;
    MainMenuAdapter mainMenuAdapter;
    ImageView userImageView;
    TextView userNameTextView;
    LoginSharedPreferences loginSharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initialization();
        set();
        menuContent();
        callAdapter();
        toHome();
        setImage();
    }

    private void initialization(){
        mainMenuModels = new ArrayList<>();
        mainMenuHeader = (View) getLayoutInflater().inflate(R.layout.main_menu_header, null);
        userImageView = (ImageView) mainMenuHeader.findViewById(R.id.userImageView);
        userNameTextView = (TextView) mainMenuHeader.findViewById(R.id.userNameTextView);
        loginSharedPreferences = new LoginSharedPreferences();
    }

    private void toHome(){
        HomeFragment homeFragment = new HomeFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.contentView, homeFragment).commit();
    }

    private void set(){
        menuListView.addHeaderView(mainMenuHeader);
        menuListView.setOnItemClickListener(this);
        userNameTextView.setText(loginSharedPreferences.getName(this));
    }

    private void setImage(){
        try {
            byte[] decodedString = Base64.decode(loginSharedPreferences.getImage(this), Base64.DEFAULT);
            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            userImageView.setImageBitmap(decodedByte);
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private void menuContent(){
        mainMenuModels.add(new MainMenuModel(getResources().getString(R.string.history), R.drawable.logo));
        mainMenuModels.add(new MainMenuModel(getResources().getString(R.string.signOut), R.drawable.logo));
    }

    private void callAdapter(){
        mainMenuAdapter = new MainMenuAdapter(this, mainMenuModels);
        menuListView.setAdapter(mainMenuAdapter);
    }

    @OnClick(R.id.menuImageView) void menuAction() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawers();
        } else {
            drawerLayout.openDrawer(GravityCompat.START);
        }
    }

    @Override
    public void onBackPressed() {
        if(drawerLayout.isDrawerOpen(menuListView)){
            drawerLayout.closeDrawer(menuListView);
        } else{
            if(getSupportFragmentManager().getBackStackEntryCount() != 0){
                FragmentManager.BackStackEntry backEntry = getSupportFragmentManager().getBackStackEntryAt(getSupportFragmentManager().getBackStackEntryCount() - 1);
                getSupportFragmentManager().popBackStack();
                getSupportFragmentManager().beginTransaction().remove(getSupportFragmentManager().findFragmentById(R.id.contentView)).commit();
            } else {
                finish();
            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        drawerLayout.closeDrawers();
        switch (i){
            case 1:
                toHistory();
                break;
            case 2:
                signOut();
                break;
        }
    }

    private void toHistory(){
        HistoryFragment historyFragment = new HistoryFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.contentView, historyFragment).addToBackStack("").commit();
    }

    private void signOut(){
        new LoginSharedPreferences().removeLogin(this);
        finish();
        Intent toHomeActivity = new Intent(this, LoginActivity.class);
        startActivity(toHomeActivity);
    }
}
