package ash.zgo.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ash.zgo.R;
import ash.zgo.database.Users;
import ash.zgo.utils.LoginSharedPreferences;
import ash.zgo.utils.Validation.ValidationObject;
import ash.zgo.utils.Validation.ViewsValidation;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends Activity {
    @BindView(R.id.usernameEditText) EditText usernameEditText;
    @BindView(R.id.passwordEditText) EditText passwordEditText;
    @BindView(R.id.rememberCheckBox) CheckBox rememberCheckBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.signInButton) void signInAction() {
        if (checkValidation()){
            checkUser();
        }
    }

    private void checkUser(){
        try {
            List<Users> usernameResult = Users.findWithQuery(Users.class, "Select * from Users where username = ? and password = ?", usernameEditText.getText().toString(),passwordEditText.getText().toString());
            if (usernameResult.size() == 0){
                Toast.makeText(this, getString(R.string.invalidUser), Toast.LENGTH_SHORT).show();
            } else {
                if (rememberCheckBox.isChecked())
                    new LoginSharedPreferences(LoginActivity.this, usernameResult.get(0).email, usernameResult.get(0).password, usernameResult.get(0).username, usernameResult.get(0).photo, "1");
                else
                    new LoginSharedPreferences(LoginActivity.this, usernameResult.get(0).email, usernameResult.get(0).password, usernameResult.get(0).username, usernameResult.get(0).photo, "0");
                toMainActivity();
            }
        } catch (Exception e){
            Toast.makeText(this, getString(R.string.invalidUser), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    public boolean checkValidation() {
        ArrayList<ValidationObject> list=new ArrayList<>();
        list.add(new ValidationObject(usernameEditText , 6, true, R.string.usernameErrorMsg));
        list.add(new ValidationObject(passwordEditText , 6, true, R.string.passwordErrorMsg));
        return ViewsValidation.getInstance().validate(this,list);
    }

    private void toMainActivity(){
        finish();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
