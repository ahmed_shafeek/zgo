package ash.zgo.models;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;

import com.google.android.gms.maps.OnMapReadyCallback;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import java.io.StringReader;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import ash.zgo.R;
import ash.zgo.database.Locations;
import ash.zgo.utils.GMapV2Direction;

/**
 * Created by ahmed.shafeek on 3/6/2017.
 */

public class MapLocationViewHolder extends RecyclerView.ViewHolder implements OnMapReadyCallback {
    public TextView title;
    public TextView distanceTextView, timeTextView;

    protected GoogleMap mGoogleMap;
    protected Locations mMapLocation;
    public MapView mapView;
    private Context mContext;
    PolylineOptions polylineOptions;
    GMapV2Direction gMapV2Direction;

    public MapLocationViewHolder(Context context, View view) {
        super(view);

        mContext = context;

        title = (TextView) view.findViewById(R.id.title);
        distanceTextView = (TextView) view.findViewById(R.id.distanceTextView);
        timeTextView = (TextView) view.findViewById(R.id.timeTextView);
        mapView = (MapView) view.findViewById(R.id.map);

        mapView.onCreate(null);
        mapView.getMapAsync(this);
    }

    public void setMapLocation(Locations mapLocation) {
        mMapLocation = mapLocation;

        // If the map is ready, update its content.
        if (mGoogleMap != null) {
            updateMapContents();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
        MapsInitializer.initialize(mContext);
        googleMap.getUiSettings().setMapToolbarEnabled(false);
        // If we have map data, update the map content.
        if (mMapLocation != null) {
            updateMapContents();
        }
    }

    protected void updateMapContents() {
        try {
            mGoogleMap.clear();
            mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble(mMapLocation.latDes), Double.parseDouble(mMapLocation.lngDes))));
            mGoogleMap.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble(mMapLocation.latSrc), Double.parseDouble(mMapLocation.lngSrc))));
            gMapV2Direction = new GMapV2Direction();
            DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(mMapLocation.document));
            ArrayList<LatLng> directionPoint = gMapV2Direction.getDirection(db.parse(is));
            polylineOptions = new PolylineOptions().width(5).color(Color.RED);
            for (int i = 0; i < directionPoint.size(); i++)
                polylineOptions.add(directionPoint.get(i));
            mGoogleMap.addPolyline(polylineOptions);
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(Double.parseDouble(mMapLocation.latDes), Double.parseDouble(mMapLocation.lngDes)), 8f);
            mGoogleMap.moveCamera(cameraUpdate);
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
