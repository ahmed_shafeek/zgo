package ash.zgo.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;

import ash.zgo.R;
import ash.zgo.database.Locations;
import ash.zgo.models.MapLocationViewHolder;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.MapView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Created by ahmed.shafeek on 3/6/2017.
 */

public class MapLocationAdapter extends RecyclerView.Adapter<MapLocationViewHolder> implements View.OnClickListener {
    protected HashSet<MapView> mMapViews = new HashSet<>();
    protected List<Locations> mMapLocations;
    Context context;
    RecyclerView recyclerView;

    public MapLocationAdapter(RecyclerView recyclerView, Context context) {
        this.context = context;
        this.recyclerView = recyclerView;
        mMapLocations = Locations.findWithQuery(Locations.class, "Select * from Locations");
    }

    public void setMapLocations(ArrayList<Locations> mapLocations) {
        mMapLocations = mapLocations;
    }

    @Override
    public MapLocationViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
        final View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.history_custom, viewGroup, false);
        MapLocationViewHolder viewHolder = new MapLocationViewHolder(context, view);
        view.setOnClickListener(this);
        mMapViews.add(viewHolder.mapView);
        return viewHolder;
    }

    @Override
    public void onClick(View v) {
        try {
            int itemPosition = recyclerView.getChildPosition(v);
            final Locations mapLocation = mMapLocations.get(itemPosition);
            final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?" + "saddr="+ mapLocation.latSrc + "," + mapLocation.lngSrc + "&daddr=" + mapLocation.latDes + "," + mapLocation.lngDes));
            intent.setClassName("com.google.android.apps.maps","com.google.android.maps.MapsActivity");
            context.startActivity(intent);
        } catch (Exception e){
            e.printStackTrace();
        }
    }


    @Override
    public void onBindViewHolder(MapLocationViewHolder viewHolder, int position) {
        final Locations mapLocation = mMapLocations.get(position);

        viewHolder.itemView.setTag(mapLocation);

        viewHolder.title.setText(mapLocation.title);
        viewHolder.timeTextView.setText("Time : " + mapLocation.time);
        viewHolder.distanceTextView.setText("Distance : " + mapLocation.distance);
        viewHolder.setMapLocation(mapLocation);
    }

    @Override
    public int getItemCount() {
        return mMapLocations == null ? 0 : mMapLocations.size();
    }

    public HashSet<MapView> getMapViews() {
        return mMapViews;
    }
}
