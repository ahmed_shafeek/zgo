package ash.zgo.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by shafeek on 04/08/16.
 */
public class LoginSharedPreferences {

    private Context context;
    private String email, password, name, image, remember;
    SharedPreferences.Editor editor;
    SharedPreferences settings;

    public LoginSharedPreferences() {
    }

    public LoginSharedPreferences(Context context, String email, String password, String name, String image, String remember) {
        this.context = context;
        this.email = email;
        this.password = password;
        this.name = name;
        this.image = image;
        this.remember = remember;
        settings = context.getSharedPreferences("login", 0);
        editor = settings.edit();
        editor.putString("email", email);
        editor.putString("password", password);
        editor.putString("name", name);
        editor.putString("image", image);
        editor.putString("remember", remember);
        editor.commit();
    }

    public String getEmail(Context context) {
        settings = context.getSharedPreferences("login", 0);
        return settings.getString("email", "");
    }

    public void setEmail(String email) {
        this.email = email;
        settings = context.getSharedPreferences("login", 0);
        editor = settings.edit();
        editor.putString("email", email);
        editor.commit();
    }

    public String getPassword(Context context) {
        settings = context.getSharedPreferences("login", 0);
        return settings.getString("password", "");
    }

    public void setPassword(String password) {
        this.password = password;
        settings = context.getSharedPreferences("login", 0);
        editor = settings.edit();
        editor.putString("password", password);
        editor.commit();
    }

    public String getName(Context context) {
        settings = context.getSharedPreferences("login", 0);
        return settings.getString("name", "");
    }

    public void setName(String name) {
        this.name = name;
        settings = context.getSharedPreferences("login", 0);
        editor = settings.edit();
        editor.putString("name", name);
        editor.commit();
    }

    public String getImage(Context context) {
        settings = context.getSharedPreferences("login", 0);
        return settings.getString("image", "");
    }

    public void setImage(String image) {
        this.image = image;
        settings = context.getSharedPreferences("login", 0);
        editor = settings.edit();
        editor.putString("image", image);
        editor.commit();
    }

    public String getRemember(Context context) {
        settings = context.getSharedPreferences("login", 0);
        return settings.getString("remember", "0");
    }

    public void setRemember(String remember) {
        this.remember = remember;
        settings = context.getSharedPreferences("login", 0);
        editor = settings.edit();
        editor.putString("remember", remember);
        editor.commit();
    }

    public void removeLogin(Context context){
        settings = context.getSharedPreferences("login", 0);
        editor = settings.edit();
        editor.remove("email");
        editor.remove("password");
        editor.remove("name");
        editor.remove("image");
        editor.remove("remember");
        editor.commit();
    }
}
