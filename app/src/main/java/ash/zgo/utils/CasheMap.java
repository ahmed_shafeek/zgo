package ash.zgo.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by shafeek on 04/08/16.
 */
public class CasheMap {

    private Context context;
    private double latSrc, lngSrc, latDes, lngDes;
    SharedPreferences.Editor editor;
    SharedPreferences settings;

    public CasheMap(Context context) {
        this.context = context;
    }

    public CasheMap(Context context, double latSrc, double lngSrc, double latDes, double lngDes) {
        this.context = context;
        this.latSrc = latSrc;
        this.lngSrc = lngSrc;
        this.latDes = latDes;
        this.lngDes = lngDes;
        settings = context.getSharedPreferences("location", 0);
        editor = settings.edit();
        editor.putString("latSrc", Double.toString(latSrc));
        editor.putString("lngSrc", Double.toString(lngSrc));
        editor.putString("latDes", Double.toString(latDes));
        editor.putString("lngDes", Double.toString(lngDes));
        editor.commit();
    }

    public double getLatSrc(Context context) {
        settings = context.getSharedPreferences("location", 0);
        return Double.parseDouble(settings.getString("latSrc", "30.015697"));
    }

    public void setLatSrc(double latSrc) {
        this.latSrc = latSrc;
        settings = context.getSharedPreferences("location", 0);
        editor = settings.edit();
        editor.putString("latSrc", Double.toString(latSrc));
        editor.commit();
    }

    public double getLngSrc(Context context) {
        settings = context.getSharedPreferences("location", 0);
        return Double.parseDouble(settings.getString("lngSrc", "31.418043"));
    }

    public void setLngSrc(double lngSrc) {
        this.lngSrc = lngSrc;
        settings = context.getSharedPreferences("location", 0);
        editor = settings.edit();
        editor.putString("lngSrc", Double.toString(lngSrc));
        editor.commit();
    }

    public double getLatDes(Context context) {
        settings = context.getSharedPreferences("location", 0);
        return Double.parseDouble(settings.getString("latDes", "30.021671"));
    }

    public void setLatDes(double latDes) {
        this.latDes = latDes;
        settings = context.getSharedPreferences("location", 0);
        editor = settings.edit();
        editor.putString("latDes", Double.toString(latDes));
        editor.commit();
    }

    public double getLngDes(Context context) {
        settings = context.getSharedPreferences("location", 0);
        return Double.parseDouble(settings.getString("lngDes", "31.411012"));
    }

    public void setLngDes(double lngDes) {
        this.lngDes = lngDes;
        settings = context.getSharedPreferences("location", 0);
        editor = settings.edit();
        editor.putString("lngDes", Double.toString(lngDes));
        editor.commit();
    }

    public void removeLocation(Context context){
        settings = context.getSharedPreferences("location", 0);
        editor = settings.edit();
        editor.remove("id");
        editor.remove("email");
        editor.remove("password");
        editor.remove("name");
        editor.remove("image");
        editor.remove("remember");
        editor.commit();
    }
}
